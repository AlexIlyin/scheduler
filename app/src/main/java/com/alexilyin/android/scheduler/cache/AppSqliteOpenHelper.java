package com.alexilyin.android.scheduler.cache;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by alexilyin on 12.07.16.
 */
public class AppSqliteOpenHelper extends SQLiteOpenHelper {

    public AppSqliteOpenHelper(Context context) {
        super(context, AppSqliteContract.DB_NAME, null, AppSqliteContract.DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(NoteTable.SQL_QUERY_CREATE_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        sqLiteDatabase.execSQL(NoteTable.SQL_QUERY_DROP_TABLE);

        onCreate(sqLiteDatabase);
    }
}
