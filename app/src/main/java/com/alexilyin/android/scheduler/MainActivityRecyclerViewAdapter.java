package com.alexilyin.android.scheduler;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by alexilyin on 12.07.16.
 */
public class MainActivityRecyclerViewAdapter extends RecyclerView.Adapter<MainActivityRecyclerViewAdapter.MainActivityRecyclerViewHolder> {


    @Override
    public MainActivityRecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_main_recyclervew, parent, false);
        return new MainActivityRecyclerViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MainActivityRecyclerViewHolder holder, int position) {
        holder.textView.setText(""+position);

    }

    @Override
    public int getItemCount() {
        return 3;
    }

    class MainActivityRecyclerViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.item_main_recyclerview_text)
        TextView textView;

        public MainActivityRecyclerViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
