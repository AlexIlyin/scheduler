package com.alexilyin.android.scheduler.model;

import com.alexilyin.android.scheduler.cache.NoteTable;
import com.pushtorefresh.storio.sqlite.annotations.StorIOSQLiteColumn;
import com.pushtorefresh.storio.sqlite.annotations.StorIOSQLiteType;

/**
 * Created by alexilyin on 12.07.16.
 */
@StorIOSQLiteType(table = NoteTable.TABLE_NAME)
public class Note {

    @StorIOSQLiteColumn(name = NoteTable._ID, key = true)
    public int id;

    @StorIOSQLiteColumn(name = NoteTable.COLUMN_NAME_TITLE)
    public String title;

    @StorIOSQLiteColumn(name = NoteTable.COLUMN_NAME_TEXT)
    public String text;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
