package com.alexilyin.android.scheduler.di;

import javax.inject.Singleton;

import dagger.Component;
import dagger.Module;

/**
 * Created by alexilyin on 12.07.16.
 */

@Singleton
@Component (modules = {AppModule.class})
public interface AppComponent {
}
