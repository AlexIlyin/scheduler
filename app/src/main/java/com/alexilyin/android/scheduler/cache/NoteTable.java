package com.alexilyin.android.scheduler.cache;

import android.provider.BaseColumns;

/**
 * Created by alexilyin on 13.07.16.
 */
public class NoteTable implements BaseColumns {

    public static final String TABLE_NAME = "note";

    public static final String COLUMN_NAME_TITLE = "Title";
    public static final String COLUMN_NAME_TEXT = "Text";

    public static final String SQL_QUERY_CREATE_TABLE = "CREATE TABLE IF NOT EXISTS " + TABLE_NAME + " ( " +
            _ID + " INTEGER PRIMARY KEY AUTOINCREMENT , " +
            COLUMN_NAME_TITLE + " TEXT NOT NULL ON CONFLICT ROLLBACK , " +
            COLUMN_NAME_TEXT + " TEXT NOT NULL ON CONFLICT ROLLBACK " +
            ")";

    public static final String SQL_QUERY_DROP_TABLE = "DROP TABLE IF EXISTS " + TABLE_NAME;
}
